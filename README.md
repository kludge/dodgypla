# DodgyPLA

Open source Commodore 64 PLA replacement. This has only been rerouted by me for PCB fabrication, original version for home etching at http://github.com/desaster/c64-dodgypla. All kudos to Upi for this.

<img src="DodgyPLA_v1.0.jpg" alt="DodgyPLA v1.0" width="33%">

Version 1.1 has not been tested yet, but there are only small changes from version 1.0. Version 1.0 has so far been tested with the following without any problems:

* C64 ASSY 250407 Rev A (with and without _CASRAM RC filter)
* C64 ASSY 250425
* 586220+ Diagnostic with harness
* EasyFlash 3 cartridge
* 1541 Ultimate-II
* Epyx Fastload Reloaded

Note that this is not at all compatible with the PLA found on the 250469 boards.

## BOM

| Reference | Type | Footprint | Value |
| --------- | ---- | --------- | ----- |
| U1 | 3V3 Voltage Regulator | SOT-223 | AMS1117-3.3 |
| U2 (unmarked) | Xilinx CPLD | VQFP-44 | Xilinx XC9536XL-10VQ44C |
| U3 (unmarked) | 2 * 14 pin turned male pin header | Pitch 2.54 mm / 0.1"| |
| C1 | Ceramic capacitor | 0805 | 3.3 uF |
| C2 | Ceramic capacitor | 0805 | 3.3 uF |
| C3 | Ceramic capacitor | 0805 | 0.1 uF / 100 nF |
| C4 | Ceramic capacitor | 0805 | 0.1 uF / 100 nF |

## Disclaimer

This project is released as is, including any typos or other errors. I take no responsibility if it damages anything or sets fire to your house.